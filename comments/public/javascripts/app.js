angular.module('comment', [])
.controller('MainCtrl', [
  '$scope','$http',
  function($scope,$http){
    $scope.test = 'Hello world!';
$scope.formContent='';
    $scope.comments = [
      {title:'Comment 1', upvotes:5},
      {title:'Comment 2', upvotes:6},
      {title:'Comment 3', upvotes:1},
      {title:'Comment 4', upvotes:4},
      {title:'Comment 5', upvotes:3}
    ];
$scope.getAll = function() {
	$http.get('/comments').success(function(d){$scope.comments = d;});
    };
    $scope.getAll();    


    $scope.addComment = function() {
	if($scope.formContent == '') { return; }
      console.log("In addComment with "+$scope.formContent);
	var time = new Date(new Date().getTime()).toString()
      $scope.create({
        Comment: $scope.formContent,
	Time:time,
        upvotes: 0
      });
      $scope.formContent = '';
      //$scope.comments.push({Comment:$scope.formContent,upvotes:0});
      $scope.formContent='';
      $scope.getAll();
    };
    $scope.incrementUpvotes = function(comment) {
      $scope.upvote(comment);
    };

    $scope.upvote = function(comment) {
      return $http.put('/comments/' + comment._id + '/upvote')
        .success(function(data){
          console.log("upvote worked");
          comment.upvotes += 1;
        });
    };

    $scope.create = function(comment) {
     return $http.post('/comments', comment).success(function(data){
       $scope.comments.push(data);
    });

    
  };
  }
]);
